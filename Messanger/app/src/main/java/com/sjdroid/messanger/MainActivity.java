package com.sjdroid.messanger;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.Telephony;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.sjdroid.messanger.settings.SettingsActivity;
import com.sjdroid.messanger.settings.settings2Activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private RecyclerView mRecyclerView;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

     /*   for (String msg:
             getAllSmsFromProvider()) {
            Log.i("message",msg);

        }
*/
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);


        // getLoaderManager().initLoader(1, null, this);
        ReadSms();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Search();
                return true;
            case R.id.settings:
                showSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } }

    private void showSettings() {
        Intent it= new Intent(this, settings2Activity.class);
        startActivity(it);

    }

    private void Search() {
    }

    private void ReadSms() {


        int hasWriteContactsPermission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasWriteContactsPermission = checkSelfPermission(Manifest.permission.READ_SMS);

            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_SMS},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }
        getLoaderManager().initLoader(0, null, this);
    }

    /*public List<String> getAllSmsFromProvider() {
        List<String> lstSms = new ArrayList<String>();
        ContentResolver cr =getContentResolver();

        Cursor c = cr.query(Telephony.Sms.Inbox.CONTENT_URI, // Official CONTENT_URI from docs
                new String[] { Telephony.Sms.Inbox.CREATOR }, // Select body text
                null,
                null,
                Telephony.Sms.Inbox.DEFAULT_SORT_ORDER); // Default sort order

        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                lstSms.add(c.getString(0));
                c.moveToNext();
            }
        } else {
            throw new RuntimeException("You have no SMS in Inbox");
        }
        c.close();

        return lstSms;
    }
*/
    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle args) {
        Log.d("", "onCreateLoader() >> loaderID : " + loaderID);

        switch (loaderID) {
            case 0:
                // Returns a new CursorLoader
                return new CursorLoader(
                        this,   // Parent activity context
                        Telephony.Threads.CONTENT_URI,        // Table to query
                        null,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        Telephony.TextBasedSmsColumns.DATE             // Default sort order
                );


            case 1:
                // Returns a new CursorLoader
                return new CursorLoader(
                        this,   // Parent activity context
                        Telephony.Sms.CONTENT_URI,        // Table to query
                        null,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        null             // Default sort order
                );
            default:
                return null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.i("position"," this i onresume ");
      //  mRecyclerView.getAdapter().notifyDataSetChanged();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("position"," this i onrestart ");
        mRecyclerView.getAdapter().notifyDataSetChanged();

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d("", "onLoadFinished()");

        StringBuilder sb = new StringBuilder();
        int i = 0;
        //  String person = data.getColumnIndex( Sms.Inbox.PERSON)
        sb.append("<h4>Call Log Details <h4>");
        sb.append("\n");
        sb.append("\n");

        sb.append("<table>");

        int j = 0;
        for (String s :
                data.getColumnNames()) {

            Log.i("colomns in thread  " + j + " :", s);
            j++;
        }
        List<MySms> messaeges = new ArrayList<MySms>();
        MySms msg = new MySms();
        while (data.moveToNext()) {
            msg = new MySms();
            String Address = data.getString(12);
            //String person = data.getString(3);
            Date date = new Date(data.getLong(4));
            String calDate = new SimpleDateFormat("HH:mm dd-MM-yyyy").format(date);
            //   Date date2 = new Date(data.getLong(4));
            //   String Date2 = new SimpleDateFormat("HH:mm dd-MM-yyyy").format(date2);
            //    String type = data.getString(9);
            //   String sub = data.getString(11);
            String body = data.getString(20);

            msg.setSender(Address);
            msg.setBody(body);
            msg.setmDate(calDate);

            messaeges.add(msg);
            msg = null;


        }

        HashSet<String> UniqueSender = new HashSet<String>();
        ArrayList<MySms> threads = new ArrayList<MySms>();

        for (MySms sm : messaeges
                ) {
            Log.i("messgee", sm.Body + " -> " + sm.mDate);

            if (!UniqueSender.contains(sm.sender))
                threads.add(sm);
            else
                UniqueSender.add(sm.sender);
        }


        MyAdapter mAdapter = new MyAdapter(threads, this);
        mRecyclerView.setAdapter(mAdapter);

      /*  TextView txt = (TextView) findViewById(R.id.hello);
        txt.setText(Html.fromHtml(sb.toString()));*/

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


}
