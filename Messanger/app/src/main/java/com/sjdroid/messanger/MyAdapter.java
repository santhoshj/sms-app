package com.sjdroid.messanger;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by SJ045822 on 5/9/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<MySms> messages;
    Context mContext;

   // HashSet<String> senders;
    MyAdapter(List<MySms> messages, Context mContext) {
        this.messages = messages;
        this.mContext = mContext;
        //senders= new HashSet<String>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.smslayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

       // String senderAdd= messages.get(position).sender;

        //if(!senders.contains(senderAdd)) {
        //    senders.add(senderAdd);
        int size= messages.size()-1;
            holder.sender.setText(messages.get(size-position).sender);
            holder.senddate.setText(messages.get(size-position).mDate);
            holder.body.setText(messages.get(size-position).Body);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "item clicked :" + holder.sender.getText(), Toast.LENGTH_LONG).show();

                    Intent it = new Intent(mContext, chatActivity.class);
                    it.putExtra("sender", holder.sender.getText());
                    mContext.startActivity(it);

                }
            });


    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView sender;
        public TextView senddate;
        public TextView body;
        public View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            sender = (TextView) itemView.findViewById(R.id.sender);
            senddate = (TextView) itemView.findViewById(R.id.timeview);
            body = (TextView) itemView.findViewById(R.id.snippet);
            mView = itemView;
        }
    }

}
