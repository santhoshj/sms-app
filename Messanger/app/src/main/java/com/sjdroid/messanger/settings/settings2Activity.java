package com.sjdroid.messanger.settings;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.sjdroid.messanger.MainActivity;
import com.sjdroid.messanger.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Iterator;

/**
 * Created by SJ045822 on 5/14/2016.
 */
public class settings2Activity extends AppCompatPreferenceActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LoaderManager.LoaderCallbacks<Cursor> {


    private GoogleApiClient mGoogleApiClient;
    private final int RESOLVE_CONNECTION_REQUEST_CODE = 123;
    private int Exists_falg = 1;
    private DriveFile driveFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_APPFOLDER)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Log.i("clicked", preference.getKey());


        switch (preference.getKey()) {
            case "example_switch":
                Log.i("position", "position checkbox");
                break;
            case "backupnow":
                final DriveId[] mDriveId = new DriveId[1];
                Log.i("position", "position 0");
                final ResultCallback<DriveApi.DriveContentsResult> contentsCallback =
                        new ResultCallback<DriveApi.DriveContentsResult>() {
                            @Override
                            public void onResult(DriveApi.DriveContentsResult result) {
                                Log.i("position", "position in");
                                if (!result.getStatus().isSuccess()) {
                                    showMessage("Error while trying to create new file contents");
                                    return;
                                }
                                Log.i("position", "position 1");
                                final MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                        .setTitle("HelloWorld.java")
                                        .setMimeType("text/plain")
                                        .build();

                                Log.i("position", "position 2");
                                Drive.DriveApi.getAppFolder(mGoogleApiClient)
                                        .createFile(mGoogleApiClient, changeSet, result.getDriveContents())
                                        .setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                                            @Override
                                            public void onResult(@NonNull DriveFolder.DriveFileResult driveFileResult) {

                                                if (!driveFileResult.getStatus().isSuccess()) {
                                                    driveFile = driveFileResult.getDriveFile();
                                                    Bundle b = new Bundle();
                                                    b.putSerializable("drivefile", (Serializable) driveFile);
                                                    getLoaderManager().initLoader(0, null, settings2Activity.this);
                                                    Context context = getApplicationContext();

                                                    SharedPreferences sharedPref = context.getSharedPreferences(
                                                            getString(R.string.app_name), Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPref.edit();
                                                    mDriveId[0] = driveFileResult.getDriveFile().getDriveId();
                                                    editor.putString(getString(R.string.driveId), mDriveId[0].toString());
                                                    editor.commit();

                                                    Log.d("driveid ", mDriveId[0] + "");
                                                    showMessage1(" test " + driveFileResult.getStatus().toString() + "created with file id" + mDriveId[0]);
                                                }
                                            }
                                        });


                            }
                        };


                Query query = new Query.Builder()
                        .addFilter(Filters.eq(SearchableField.TITLE, "HelloWorld.java"))
                        .build();

                Drive.DriveApi.getAppFolder(mGoogleApiClient).queryChildren(mGoogleApiClient, query)
                        .setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                            public DriveContents driveContents;

                            @Override
                            public void onResult(@NonNull DriveApi.MetadataBufferResult metadataBufferResult) {

                                if (!metadataBufferResult.getStatus().isSuccess()) {
                                    showMessage("file creation failed");
                                    return;
                                }


                                if (metadataBufferResult.getMetadataBuffer().getCount() < 1) {
                                    showMessage1("file not Exists creating new file ");

                                    Drive.DriveApi.newDriveContents(mGoogleApiClient)
                                            .setResultCallback(contentsCallback);

                                } else {
                                    driveFile = metadataBufferResult.getMetadataBuffer().get(0).getDriveId().asDriveFile();
                                    getLoaderManager().initLoader(0, null, settings2Activity.this);
                                    showMessage1("file found with id:" + driveFile.getDriveId());
                                }
                                if (driveContents != null)
                                    driveContents.commit(mGoogleApiClient, null).setResultCallback(new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(Status result) {
                                            // Handle the response status
                                        }
                                    });
                            }
                        });


                break;
            case "restorenow":

                DriveFolder folder = Drive.DriveApi.getAppFolder(mGoogleApiClient);
                Query query1 = new Query.Builder()
                        .addFilter(Filters.eq(SearchableField.TITLE, "HelloWorld.java"))
                        .build();


                Drive.DriveApi.getAppFolder(mGoogleApiClient).queryChildren(mGoogleApiClient, query1).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                    public DriveContents contents;

                    @Override
                    public void onResult(@NonNull DriveApi.MetadataBufferResult metadataBufferResult) {
                        if (!metadataBufferResult.getStatus().isSuccess()) {
                            showMessage("cannot get the backup");

                        }
                        driveFile = metadataBufferResult.getMetadataBuffer().get(0).getDriveId().asDriveFile();
                        driveFile.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, new DriveFile.DownloadProgressListener() {
                            @Override
                            public void onProgress(long l, long l1) {
                                Log.i("first" + l, ": second" + l1);
                            }
                        })
                                .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {

                                    @Override
                                    public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                                        if (!driveContentsResult.getStatus().isSuccess()) {
                                            // display an error saying file can't be opened
                                            return;
                                        }
                                        // DriveContents object contains pointers
                                        // to the actual byte stream
                                        contents = driveContentsResult.getDriveContents();
                                        BufferedReader reader = new BufferedReader(new InputStreamReader(contents.getInputStream()));
                                        StringBuilder builder = new StringBuilder();
                                        String line;
                                        try {
                                            while ((line = reader.readLine()) != null) {
                                                builder.append(line);
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        String contentsAsString = builder.toString();
                                        try {
                                            JSONArray jsonArray = new JSONArray(contentsAsString);
                                            RestoreMessage(jsonArray);
                                        } catch (JSONException e) {
                                            showMessage("mone pani paalli");
                                            e.printStackTrace();
                                        }
                                        showMessage(contentsAsString);
                                    }
                                });
                        if (contents != null)
                            contents.commit(mGoogleApiClient, null).setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status result) {
                                    // handle the response status
                                }
                            });

                    }

                });


//  file.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null)
//                        .setResultCallback(contentsOpenedCallback);
                folder.listChildren(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                    @Override
                    public void onResult(@NonNull DriveApi.MetadataBufferResult metadataBufferResult) {
                        if (!metadataBufferResult.getStatus().isSuccess()) {
                            showMessage("Problem while retrieving files");
                            return;

                        }
                        DriveId i = metadataBufferResult.getMetadataBuffer().get(0).getDriveId();
                        Log.d("restored drive id", i + "");
                        showMessage("filec search result" + metadataBufferResult.getMetadataBuffer().get(0).getDriveId() + "");


                    }
                });


                folder.getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>() {
                    @Override
                    public void onResult(@NonNull DriveResource.MetadataResult metadataResult) {
                        if (!metadataResult.getStatus().isSuccess()) {
                            Toast.makeText(getApplicationContext(), "metadata null", Toast.LENGTH_SHORT).show();
                        }
                        Metadata metadata = metadataResult.getMetadata();
                        Toast.makeText(getApplicationContext(), metadata.isFolder() + "", Toast.LENGTH_SHORT).show();


                    }
                });

                Log.i("the folder", "");
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void RestoreMessage(JSONArray jsonArray) {

        ContentValues values = new ContentValues();


        for (int i = 0; i < jsonArray.length(); i++) {
            values = new ContentValues();
            JSONObject object = jsonArray.optJSONObject(i);
            Iterator<String> iterator = object.keys();
            while (iterator.hasNext()) {
                String currentKey = iterator.next();
              //  Log.d("the key", currentKey);
                try {
                    if(!(currentKey.equals("deleted")||currentKey.equals("sync_state")))
                    values.put(currentKey, (String) object.get(currentKey));
                } catch (JSONException e) {
                    showMessage("restore failed");
                    e.printStackTrace();
                }
            }

            getContentResolver().insert(Telephony.Sms.CONTENT_URI, values);
            values = null;

        }

        Intent it= new Intent(this, MainActivity.class);
        startActivity(it);

        //values.put("address", sender);    //set the phone number that we want
        //values.put("body", body);    // body of the text that we are going to add to inbox/sent items
      //  getContentResolver().insert(Uri.parse("content://sms/inbox"), values);


    }


    private void writeToDrive(DriveFile driveFile, final JSONArray jsarry) {

        driveFile.open(mGoogleApiClient, DriveFile.MODE_WRITE_ONLY, null).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
            DriveContents driveContents;

            @Override
            public void onResult(DriveApi.DriveContentsResult result) {
                if (!result.getStatus().isSuccess()) {
                    // Handle error
                    return;
                }
                driveContents = result.getDriveContents();
                OutputStream outputStream = driveContents.getOutputStream();
                try {
                    outputStream.write(jsarry.toString().getBytes());
                    showMessage("writting to file ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                driveContents.commit(mGoogleApiClient, null).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        showMessage("writting to file completed");
                    }
                });
                //showMessage("file commited :"+mStatus);

            }
        });
    }

    private void showMessage1(String s) {

        //Log.i("the status", s);
        Toast.makeText(getApplicationContext(), s + "", Toast.LENGTH_SHORT).show();
    }

    private void showMessage(String s) {

        //Log.i("the status", s);
        Toast.makeText(getApplicationContext(), s + "", Toast.LENGTH_SHORT).show();
    }

    ResultCallback<DriveApi.DriveContentsResult> contentsOpenedCallback =
            new ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        Toast.makeText(getApplicationContext(), "failed to get files", Toast.LENGTH_SHORT).show();
                        // display an error saying file can't be opened
                        return;
                    }
                    // DriveContents object contains pointers
                    // to the actual byte stream
                    DriveContents contents = result.getDriveContents();
                }
            };

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.i("status", "connected");
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i("status", "connection Susupended");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i("status", "connection failed");

        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, RESOLVE_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                // Unable to resolve, message user appropriately
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
        }

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }


    public JSONArray cur2Json(Cursor cursor) {


        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        rowObject.put(cursor.getColumnName(i),
                                cursor.getString(i));
                    } catch (Exception e) {
                        Log.d("exception", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }


        return resultSet;

    }


    @Override
    public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,   // Parent activity context
                Telephony.Sms.CONTENT_URI,        // Table to query
                null,     // Projection to return
                null,            // No selection clause
                null,            // No selection arguments
                null             // Default sort order
        );
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {

//       JSONArray jsarry=cur2Json(data);


        JSONArray resultSet = new JSONArray();
        data.moveToFirst();
        while (data.isAfterLast() == false) {
            int totalColumn = data.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (data.getColumnName(i) != null) {
                    try {
                        rowObject.put(data.getColumnName(i),
                                data.getString(i));
                    } catch (Exception e) {
                        Log.d("exception", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            data.moveToNext();
        }


        writeToDrive(driveFile, resultSet);
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {

    }
}
