package com.sjdroid.messanger;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SJ045822 on 5/11/2016.
 */
public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder> {


    private final List<MySms> messages;

    ConversationAdapter(List<MySms> messages) {
        this.messages = messages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.converstion, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int size = messages.size() - 1;

        holder.msg_body.setText(messages.get(size - position).Body);
        holder.msg_DaT.setText(messages.get(size - position).mDate);

        switch (messages.get(size - position).type) {
            case 1:
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.layout.getLayoutParams();
                params.gravity = Gravity.END;
                holder.layout.setLayoutParams(params);

            case 4:

                LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) holder.layout.getLayoutParams();
                params1.gravity = Gravity.START;
                holder.layout.setLayoutParams(params1);

        }

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView msg_body;
        public TextView msg_DaT;
        public View layout;

        public ViewHolder(View itemView) {
            super(itemView);

            msg_body = (TextView) itemView.findViewById(R.id.msgbody);
            msg_DaT = (TextView) itemView.findViewById(R.id.msgdat);
            layout = itemView.findViewById(R.id.layouttype);
        }
    }

}
