package com.sjdroid.messanger;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class chatActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private String sender;
    private RecyclerView mRecyclerView;
    private ConversationAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent it = getIntent();
        sender = it.getStringExtra("sender");
        getSupportActionBar().setTitle(sender);

        mRecyclerView = (RecyclerView) findViewById(R.id.conversationview);
        assert mRecyclerView != null;
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        getLoaderManager().initLoader(1, null, this);

        Button send = (Button) findViewById(R.id.sendbutton);
        assert send != null;
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendsms();
            }
        });


    }

    private void sendsms() {
        EditText txt = (EditText) findViewById(R.id.textmessage);
        String message = txt.getText().toString();
        txt.clearFocus();
        try {
         //   SmsManager smsManager = SmsManager.getDefault();
           // smsManager.sendTextMessage(sender, null, message, null, null);

            SendsmsAsyncTask task = new SendsmsAsyncTask();
            task.execute(new String[] { message});

            //getLoaderManager().initLoader(1, null, this);
            mAdapter.notifyDataSetChanged();

        } catch (Exception e) {

            Log.d("execption raised", e.getMessage());
        }

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {


        CursorLoader cr = new CursorLoader(
                this,   // Parent activity context
                Telephony.Sms.CONTENT_URI,        // Table to query
                null,     // Projection to return
                null,            // No selection clause
                null,            // No selection arguments
                null             // Default sort order
        );
        cr.setSelection("address='" + sender + "'");
        // cr.setSortOrder("date ASC");
        Log.i("selection", cr.getSelection() + "");
        return cr;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {


        ArrayList<MySms> messaeges = new ArrayList<MySms>();

        MySms msg;
        while (data.moveToNext()) {
            msg = new MySms();


            String Address = data.getString(2);
            String person = data.getString(3);
            Date date = new Date(data.getLong(4));
            String calDate = new SimpleDateFormat("HH:mm dd-MM-yyyy").format(date);
            Date date2 = new Date(data.getLong(4));
            String Date2 = new SimpleDateFormat("HH:mm dd-MM-yyyy").format(date2);
            int type = data.getInt(9);
            String sub = data.getString(11);
            String body = data.getString(12);


            msg.setSender(Address);
            msg.setBody(body);
            msg.setmDate(calDate);
            msg.setType(type);

            messaeges.add(msg);
            msg = null;


        }

     /*   HashSet<String> UniqueSender = new HashSet<String>();
        ArrayList<MySms> threads = new ArrayList<MySms>();

        for (MySms sm : messaeges
                ) {
            Log.i("messgee", sm.Body + " -> " + sm.mDate);

            if (!UniqueSender.contains(sm.sender))
                threads.add(sm);
            else
                UniqueSender.add(sm.sender);
        }
*/

        mAdapter = new ConversationAdapter(messaeges);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.getLayoutManager().scrollToPosition(mAdapter.getItemCount()-1);



    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    private class SendsmsAsyncTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(sender, null, params[0], null, null);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            Log.i("message","message sent");
            super.onPostExecute(s);
            mAdapter.notifyDataSetChanged();
            EditText mEditText= (EditText) findViewById(R.id.textmessage);
            mEditText.setText("");

        }
    }


}
